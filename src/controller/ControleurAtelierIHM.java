package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import modele.Atelier;
import modele.Personne;

public class ControleurAtelierIHM {
	
	private Atelier atelier;
	private int indiceAtelierEnCours;
	private ObservableList <Atelier> ateliers;
	private ObservableList <Personne> personnes;
	/*
	 * ateliers de ObservablesList 
	 * va contenir les ateliers et va être
	 * observé par tabAtelier de TableView
	 * qui va afficher les ateliers;
	 */
	
	// Partie Atelier
	@FXML private TextField saisieNomAtelier;
	@FXML private TextField saisieDescription;
	@FXML private TextField saisieCapaciteMax;
	@FXML private TextField saisieCapaciteMin;
	@FXML private Label nbPlacesDisponibles;
	@FXML private Button nouvelAtelier;
	@FXML private Button modif;
	@FXML private Button supprimer;
	@FXML private TableView<Atelier> tabAtelier;
	@FXML private TableColumn<Atelier,String> nomAtelier;
	@FXML private TableColumn<Atelier,String> colDescription;
	@FXML private TableColumn<Atelier,Number> colMinPlaces;
	@FXML private TableColumn<Atelier,Number> colMaxPlaces;
	@FXML private TableColumn<Atelier,Number> colPlacesDispo;
	
	// Partie Personne
	@FXML private TextField saisieNom;
	@FXML private TextField saisiePrenom;
	@FXML private TextField saisieTel;
	@FXML private TableView<Personne> tabPersonne;
	@FXML private TableColumn<Personne,String> nomPersonne;
	@FXML private TableColumn<Personne,String> prenomPersonne;
	@FXML private TableColumn<Personne,Number> telPersonne;
	
	
	public ControleurAtelierIHM() {
		this.ateliers = FXCollections.observableArrayList();
		this.personnes = FXCollections.observableArrayList();
		this.indiceAtelierEnCours = -1;
	}
	@FXML
	// Ajouter un atelier en cliquant sur Ajouter Atelier
	public void ajouterAtelier(ActionEvent event) {
		
		atelier = recupererAtelierSaisie();
		ateliers.add(atelier);
		effacerChampsSaisies();
	}	
	
	@FXML
	// Modifier un atelier en cliquant sur Modifier Atelier
	public void modifierAtelier(ActionEvent event) {
		atelier = recupererAtelierSaisie();
		ateliers.set(indiceAtelierEnCours, atelier);
		effacerChampsSaisies();
	}
	
	@FXML
	// Supprimer un atelier en cliquant sur Supprimer Atelier
	public void supprimerAtelier(ActionEvent event) {
		
		this.ateliers.remove(indiceAtelierEnCours);
		effacerChampsSaisies();
	}
	
	public void initialize() {
		

		
		// Atelier
		this.nomAtelier.setCellValueFactory(cellData -> 
		cellData.getValue().nomAtelierProperty()); 
		
		this.colDescription.setCellValueFactory(cellData ->
		cellData.getValue().descriptionAtelierProperty()); 
		
		this.colMinPlaces.setCellValueFactory(cellData ->
		cellData.getValue().capaciteMinProperty()); 
		
		this.colMaxPlaces.setCellValueFactory(cellData ->
		cellData.getValue().capaciteMaxProperty());
		
		this.tabAtelier.setItems(this.ateliers);
		
		tabAtelier.getSelectionModel().selectedItemProperty().addListener( 
				(observable, oldValue, newValue) -> {
					afficherAtelier(newValue) ;
					this.indiceAtelierEnCours = ateliers.indexOf(newValue);
				});
		
		// Personne
		this.nomPersonne.setCellValueFactory(cellData -> 
			cellData.getValue().nomProperty()); 
		
		this.prenomPersonne.setCellValueFactory(cellData -> 
			cellData.getValue().prenomProperty()); 

		this.telPersonne.setCellValueFactory(cellData ->
			cellData.getValue().telProperty()); 
		
		this.tabPersonne.setItems(this.personnes);
		
		tabPersonne.getSelectionModel().selectedItemProperty().addListener( 
				(observable, oldValue, newValue) -> {
					afficherPersonne(newValue) ;
		});
	}
	
	public void afficherPersonne(Personne newValue) {
		saisieNom.setText(newValue.nomProperty().get());
		saisiePrenom.setText(newValue.prenomProperty().get());
		saisieTel.setText(newValue.telProperty().getValue().toString());
		
	}
	public void afficherAtelier(Atelier newValue) {
		saisieNomAtelier.setText(newValue.nomAtelierProperty().get());
		saisieDescription.setText(newValue.descriptionAtelierProperty().get());
		saisieCapaciteMin.setText(newValue.capaciteMinProperty().getValue().toString());
		saisieCapaciteMax.setText(newValue.capaciteMaxProperty().getValue().toString());
		
	}
	
	// fonctions de message d'erreur
	public void alerteMessage(String header, String content) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("MESSAGE D'ERREUR");
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.showAndWait();
	}
	
	public Atelier recupererAtelierSaisie() {
		
//		boolean a = true;
//		while (a) {
//			if (Integer.parseInt(this.saisieCapaciteMax.getText()) 
//			   <= Integer.parseInt(this.saisieCapaciteMin.getText())) {
//				alerteMessage("Erreur de saisie des champs","La capacité  Maximale "
//						+ "doit être "
//						+ "strictement\n supérieure à la capacité minimale");
//				effacerChampsSaisies();
//			} else if (saisieNomAtelier.getText().isEmpty()
//					   || saisieDescription.getText().isEmpty()
//					   || saisieCapaciteMin.getText().isEmpty()
//					   || saisieCapaciteMax.getText().isEmpty()){
//				alerteMessage("Champs vides", "Veuillez remplir tous les champs");
//				effacerChampsSaisies();
//			}
//		}
		
			String nomAtelier = saisieNomAtelier.getText();
			String description = saisieDescription.getText();
			int capaciteMax = Integer.parseInt(this.saisieCapaciteMax.getText());
			int capaciteMin = Integer.parseInt(this.saisieCapaciteMin.getText());
			
			Atelier atelier = new Atelier(capaciteMax,capaciteMin,description,nomAtelier);
		
		return atelier;
	}
	
	public void effacerChampsSaisies() {
		saisieNomAtelier.clear();
		saisieDescription.clear();
		saisieCapaciteMin.clear();
		saisieCapaciteMax.clear();
	}
	
	
	/*
	 * CLASSE PERSONNE
	 */
	// ajouter un participant
	public void ajoutParticipant(ActionEvent event) {
		
		if (indiceAtelierEnCours == 1) {
			throw new Error("Veuillez selectionner un atelier");
		} else {
			Personne personne = recupererPersonneSaisie();		
			personnes.add(personne);
			effacerChampsSaisies();
		}
	}
	
	// retirer un participant
	public void retirParticipant(ActionEvent event) {
		this.personnes.remove(indiceAtelierEnCours);
		effacerChampsSaisies();
	}
	
	public Personne recupererPersonneSaisie() {
		
		String prenom = saisieNom.getText();
		String nom = saisiePrenom.getText();
		long tel = Long.parseLong(saisieTel.getText());
		Personne pers = new Personne(prenom, nom, tel);
		
		return pers;
	}
	
}















