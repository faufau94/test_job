package modele;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Personne {

	private StringProperty prenom;
	private StringProperty nom;
	private LongProperty tel;
	
	public Personne(String prenom, String nom,long tel) {
		this.prenom = new SimpleStringProperty(prenom);
		this.nom = new SimpleStringProperty(nom);
		this.tel = new SimpleLongProperty(tel);;
	}

	public final StringProperty prenomProperty() {
		return this.prenom;
	}

	public final String getPrenom() {
		return this.prenomProperty().get();
	}
	

	public final void setPrenom(final String prenom) {
		this.prenomProperty().set(prenom);
	}
	

	public final StringProperty nomProperty() {
		return this.nom;
	}
	

	public final String getNom() {
		return this.nomProperty().get();
	}
	

	public final void setNom(final String nom) {
		this.nomProperty().set(nom);
	}
	

	public final LongProperty telProperty() {
		return this.tel;
	}
	

	public final long getTel() {
		return this.telProperty().get();
	}
	

	public final void setTel(final long tel) {
		this.telProperty().set(tel);
	}
	

	
	
	
	
	
}
