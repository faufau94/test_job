package modele;

import java.util.ArrayList;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Atelier {

	private ArrayList<Personne> equipe;
	private StringProperty nomAtelier;
	private StringProperty description;
	private IntegerProperty nbPlacesDisponibles;
	private IntegerProperty capaciteMax;
	private IntegerProperty capaciteMin;
	
	public Atelier(int capaciteMax,int capaciteMin,String description, String nomAtelier) {
		
		this.capaciteMax = new SimpleIntegerProperty(capaciteMax);
		this.capaciteMin = new SimpleIntegerProperty(capaciteMin);
		this.nomAtelier = new SimpleStringProperty(nomAtelier);
		this.description = new SimpleStringProperty(description);
		this.nbPlacesDisponibles = new SimpleIntegerProperty(capaciteMax);
	}
	
	// Capacité Min qui renvoie int
	public int getCapaciteMin() {
		return this.capaciteMin.get();

	}
	// Capacité Min renvoie IntegerProperty
	public IntegerProperty capaciteMinProperty() {
		return this.capaciteMin;
	}
	
	// Capacité Max renvoie int
	public int getCapaciteMax() {
		return this.capaciteMax.get();
		
	}

	// Capacité Max renvoie IntegerProperty
	public IntegerProperty capaciteMaxProperty() {
		return this.capaciteMax;
	}
	
	// nomAtelierProperty renvoie StringProperty
	public StringProperty nomAtelierProperty() {
		return this.nomAtelier;
	}

	// PlaceDispoProperty renvoie IntegerProperty
	public IntegerProperty placesDisponiblesProperty() {
		return this.nbPlacesDisponibles;
	}
	
	// nbPlacesDisponibles renvoie int
	public int nbPlacesDisponibles() {
		return this.nbPlacesDisponibles.get();
	}
	
	// descriptionAtelierProperty renvoie IntegerProperty
	public StringProperty descriptionAtelierProperty() {
		return this.description;
	}
	
	public void modifierCapacite(int nouvelleCapacite) {
		this.capaciteMax.set(nouvelleCapacite);
	}
	
//	public void calculerNbPlacesDisponibles() {
//		
//	}
	
	public boolean activitePeutDemarrer() {
		
		if (nbPlacesDisponibles() == 0) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public void ajouterParticipant(Personne pers) {
		equipe.add(pers);
	}
	
	public ArrayList<Personne> equipe() {
		return equipe;
	}
	
//	public String toString() {
//		return "";
//	}
	
	
	
	
	
	
	
	
	
	
}
