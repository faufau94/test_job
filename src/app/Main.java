package app;

import java.io.File;
import java.net.URL;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {
	
	@Override
	public void start(Stage window) throws Exception {
		
		try {
			FXMLLoader loader = new FXMLLoader();
			URL url = new File("src/vue/AtelierIHM.fxml").toURI().toURL();
			loader.setLocation(url);
			System.out.println(loader.getLocation());
			window.setTitle("Atelier");
			BorderPane layout = new BorderPane();
			layout = loader.load();
			Scene scene = new Scene(layout,1280,720);
			window.setScene(scene);
			window.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
}

